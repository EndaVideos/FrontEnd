# EndaVideos

## User

### 1. Register
### 2. Login
### 3. Post videos
### 4. Watch videos
### 5. Qualify
### 6. Sections
### 7. Search videos
### 8. Plus "Slack Notifications".

## Tools

  - Taiga
    - https://tree.taiga.io/project/cegard-endava-final-challenge/backlog
  - GitLab
  - MySQL/Mongo
  - HTML, CSS, JS
  - Java/Python
  - Linux

## Scrum Methodology

  - Monday 10:00 to 10:15
  - Wednesday 10:00 to 10:15
  - Friday 2:30 to 2:45
