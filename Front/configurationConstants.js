const configurationConstants = {
	user: {
		nick: "lzarate",
		id: 1
	},
	backendURL: "ip-10-0-3-225.ec2.internal:8080",
	videosServer: "https://spuerto-personalchallenge.s3.amazonaws.com",
	bucketName: "spuerto-personalchallenge"
};

module.exports.configurationConstants = configurationConstants;