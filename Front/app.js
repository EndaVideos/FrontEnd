const express = require('express');
const path = require('path');
const bodyParser = require("body-parser");
const api = require('./api');
const app = express();
const logger = require('morgan');
const nunjucks = require('nunjucks');
const request = require('request');
const AWS = require('aws-sdk');
const configurationConstants = require('./configurationConstants.js').configurationConstants;
const bucketName = configurationConstants.bucketName;

const port = process.env.PORT || 3000;

AWS.config.loadFromPath('./awsConfig.json');
const s3 = new AWS.S3();


nunjucks.configure("public/views", {
  autoscape: true,
  express: app
});


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api', api);
app.use(logger('dev'));

app.set('port', port);


app.listen(port, function() {
  console.log('Endava videos front is available at http://localhost:' + port);
});


app.get('/play/:videoID', function (req, res) {
  request({
      url: configurationConstants.backendURL + "/video/" + req.params.videoID,
      json: true
    }, function (error, response, body) {
      
      if (!error && response.statusCode === 200) {
        res.render("videoPlay.html", {
          video: {
            id: req.params.videoID,
            title: body.title,
            usefulsCount: body.useful,
            notUsefulsCount: body.notUseful,
            description: body.description
          },
          videosServerURL: configurationConstants.videosServer
        });
      }
   });
});


app.get('/', function (req, res) {
  request({
      url: configurationConstants.backendURL + "/videos/",
      json: true
    }, function (error, response, body) {
      
      if (!error && response.statusCode === 200) {
        res.render("home.html", {
          resultMessage: "Specially for you",
          videos: body,
          videosServerURL: configurationConstants.videosServer
        });
      }
   });
});


app.get('/upload', function (req, res) {
  res.render("uploadVideo.html");
});


app.post('/upload', function (req, res) {
  
  
  
  var newVideoInfo = {
    title: req.body.title,
    userNick: configurationConstants.user.nick,
    description: req.body.description
  };
  
  var options = {
    url: configurationConstants.backendURL + '/video',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    json: newVideoInfo
  };
  
  request(options, function(err, httpResponse, body){
    
    var videoName = body.id + '.mp4';
    var videoFile = req.body.videoFile;
    var params = {Bucket: bucketName, Key: videoName, Body: videoFile, ACL: 'public-read'};
    s3.putObject(params, function(err, data) {
      
      if (!err)
        res.render("videoPlay.html", {
          video: {
            id: body.id,
            title: body.title,
            usefulsCount: body.useful,
            notUsefulsCount: body.notUseful,
            description: body.description
          },
          videosServerURL: configurationConstants.videosServer
        });
     });
  });
});


app.get('/search', function(req, res){
  request({
      url: configurationConstants.backendURL + "/video?videoToSearch=" + req.query.searchField,
      json: true
    }, function (error, response, body) {
      
      if (!error && response.statusCode === 200) {
        res.render("home.html", {
          resultMessage: "Your results",
          videos: body,
          videosServerURL: configurationConstants.videosServer
        });
      }
   });
});