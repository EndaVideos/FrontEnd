#!/bin/bash
set -x
rm log.log
touch log.log
server_node=`pgrep node`
if [ -z "$server_node" ]
then
  echo "No process"
else
  kill -9 $server_node
fi
npm install
node app.js >> ~/log.log 2>&1 &
server_node=`pgrep node`
echo $server_node
